package system.authentication;

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import system.users.model.User;
import system.users.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ivan on 7/21/16.
 */
@Component
public class MyAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    UserService userService;

    @Autowired
    private MessageSource messageSource; 

    private static final Logger logger = LoggerFactory.getLogger(MyAuthenticationProvider.class);

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {

        String username = auth.getName();
        String password = auth.getCredentials().toString();
        String role = "";
        EmailValidator validator = new EmailValidator();
        if (!validator.isValid(username, null) || password == null || password.length() < 6) {
            throw new BadCredentialsException(this.messageSource.getMessage("username.format.invalid", null, Locale.getDefault()));
        }
        if (userService.isEmpty()) {
            userService.addUserByCredentials(username, password, "ROLE_ADMIN");
            role = "ROLE_ADMIN";
        } else {
            User currentUser = userService.getUserByEmailAddress(username);
            if (currentUser == null
                    || !BCrypt.hashpw(password, currentUser.getSalt()).equals(currentUser.getPassword())) {
                throw new BadCredentialsException(this.messageSource.getMessage("login.invalid", null, Locale.getDefault()));
            }
            role = currentUser.getRole();
        }
        SimpleGrantedAuthority right = new SimpleGrantedAuthority(role);
        List<SimpleGrantedAuthority> rights = new ArrayList<SimpleGrantedAuthority>();
        rights.add(right);
        return new UsernamePasswordAuthenticationToken(username, password, rights);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
