package system.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by ivan on 2/13/17.
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "You do not have permission to see or use this")
public class ForbiddenException extends RuntimeException{

    private static final long serialVersionUID = -7714090744592737657L;
}
