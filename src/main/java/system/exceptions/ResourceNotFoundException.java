package system.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The requested object does not exist.")
public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 2024282751587120887L;
}
