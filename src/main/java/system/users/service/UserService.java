package system.users.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.thymeleaf.TemplateEngine;
import system.users.model.*;

import java.util.*;
import java.util.function.Function;

/**
 * Created by ivan on 7/22/16.
 */
@Service
public class UserService {

  private static final Logger logger = LoggerFactory.getLogger(UserService.class);

  @Autowired
  UserRepository repository;

  @Autowired
  PasswordTokenRepository passwordTokenRepository;

  @Autowired
  private MessageSource messageSource;

  public List<User> getAllUsers() {
    return this.repository.findAll();
  }

  public void initializeUsers() {
    this.repository.deleteAll();
  }

  public User getUserById(String id) {
    return this.repository.findById(id).get();
  }

  public User getUserByEmailAddress(String emailAddress) {
    return this.repository.findByEmailAddressIgnoreCase(emailAddress);
  }

  public boolean checkUserById(String id) {
    return this.repository.existsById(id);
  }

  public User saveUser(User user) {
    return this.repository.save(user);
  }

  public void deleteUserById(String id) {
    User toBeDeleted = this.getUserById(id);
    this.repository.delete(toBeDeleted);
  }

  public boolean isEmpty() {
    return (this.repository.count() == 0);
  }

  public void addUser(UserDTO user, Authentication auth) {
    User newUser = user.toUser();
    newUser.setSalt(BCrypt.gensalt(12));
    newUser.setPassword(BCrypt.hashpw(newUser.getPassword(), newUser.getSalt()));
    newUser.setCreatedBy(this.getUserByEmailAddress(auth.getName()).getId());
    newUser.setUpdatedBy(this.getUserByEmailAddress(auth.getName()).getId());
    newUser = this.saveUser(newUser);
  }

  public void addUserByCredentials(String emailAddress, String password, String role) {
    User user = new User();
    user.setEmailAddress(emailAddress);
    user.setSalt(BCrypt.gensalt(12));
    user.setPassword(BCrypt.hashpw(password, user.getSalt()));
    user.setRole(role);
    this.saveUser(user);
  }

  public boolean uniqueEmail(String emailAddress) {
    return this.getUserByEmailAddress(emailAddress) == null;
  }

  public void updateEmail(Authentication auth, String newEmail) {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    SimpleGrantedAuthority right = new SimpleGrantedAuthority(this.getUserByEmailAddress(auth.getName()).getRole());
    List<SimpleGrantedAuthority> rights = new ArrayList<SimpleGrantedAuthority>();
    rights.add(right);
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(newEmail, auth.getCredentials(), rights));
  }

  public JSONObject validate(UserDTO edited, String originalEmail, BindingResult bindingResult, Locale locale) {
    JSONObject response = new JSONObject();
    JSONArray errors = new JSONArray();

    List<FieldError> validationErrors = bindingResult.getFieldErrors();
    for (FieldError error : validationErrors) {
      if (error.getRejectedValue() != null) {
        JSONObject errorInField = new JSONObject();
        errorInField.put("field", error.getField());
        errorInField.put("defaultMessage", error.getDefaultMessage());
        errors.put(errorInField);
      }
    }
    if (edited.getEmailAddress() != null && !uniqueEmail(edited.getEmailAddress())) {
      JSONObject errorInField = new JSONObject();
      errorInField.put("defaultMessage", this.messageSource.getMessage("email.registered", null, locale));
      errors.put(errorInField);
    }

    response.put("errors", errors);
    if (errors.length() != 0) {
      response.put("accept", "0");
    } else {
      response.put("accept", "1");
    }
    return response;
  }

  public void updateUser(UserDTO updated, User previous, Authentication auth) {
    previous.update(updated);
    previous.setUpdatedBy(this.getUserByEmailAddress(auth.getName()).getId());
    this.saveUser(previous);
  }

  public void removeTeam(String teamName) {
    List<User> allUsers = this.getAllUsers();
    for (User user : allUsers) {
      user.removeTeam(teamName);
      this.saveUser(user);
    }
  }

  public JSONObject changePassword(String emailAddress, PasswordChange change, Locale locale) {
    User currentUser = this.getUserByEmailAddress(emailAddress);
    JSONObject response = new JSONObject();
    JSONArray errors = new JSONArray();
    if (!BCrypt.hashpw(change.getOldPassword(), currentUser.getSalt()).equals(currentUser.getPassword())) {
      JSONObject error = new JSONObject();
      error.put("defaultMessage", this.messageSource.getMessage("password.current", null, locale));
      errors.put(error);

    }
    if (!change.getNewPassword1().equals(change.getNewPassword2())) {
      JSONObject error = new JSONObject();
      error.put("defaultMessage", this.messageSource.getMessage("password.noMatch", null, locale));
      errors.put(error);
    }
    response.put("errors", errors);
    response.put("accept", "0");
    if (errors.length() != 0) {
      return response;
    }
    response.put("accept", "1");
    currentUser.setSalt(BCrypt.gensalt(12));
    currentUser.setPassword(BCrypt.hashpw(change.getNewPassword1(), currentUser.getSalt()));
    this.saveUser(currentUser);
    return response;
  }

  public String getTokenById(String id) {
    if (this.passwordTokenRepository.findByUserID(id) == null)
      return "";
    return this.passwordTokenRepository.findByUserID(id).getToken();
  }

  public void expireToken(String id) {
    if (this.passwordTokenRepository.findByUserID(id) != null) {
      PasswordToken passwordToken = this.passwordTokenRepository.findByUserID(id);
      passwordToken.setExpirationTime(new Date(System.currentTimeMillis() - 1));
      this.passwordTokenRepository.save(passwordToken);
    }
  }

  @Autowired
  private TemplateEngine templateEngine;

  @Value("${site.address}")
  private String baseURL;

  public void resetPassword(String emailAddress, Boolean toSendMail) {

    User user = this.getUserByEmailAddress(emailAddress);
    PasswordToken passwordToken = passwordTokenRepository.findByUserID(user.getId());
    if (passwordToken != null) {
      this.passwordTokenRepository.delete(passwordToken);
    }

    Integer tokenDuration = 2;
    passwordToken = new PasswordToken(this.getUserByEmailAddress(emailAddress).getId(), tokenDuration);
    this.passwordTokenRepository.save(passwordToken);

  }

  public JSONObject userResetPassword(String userID, String token, PasswordChange newPassword, Locale locale) {

    JSONObject response = new JSONObject();
    JSONArray errors = new JSONArray();

    PasswordToken passwordToken = this.passwordTokenRepository.findByUserID(userID);
    if (passwordToken == null || !passwordToken.getToken().equals(token)
        || passwordToken.getExpirationTime().getTime() < System.currentTimeMillis()) {

      JSONObject invalidToken = new JSONObject();
      invalidToken.put("defaultMessage", this.messageSource.getMessage("token.invalid", null, locale));
      errors.put(invalidToken);
    }

    if (!newPassword.getNewPassword1().equals(newPassword.getNewPassword2())) {
      JSONObject doNotMatch = new JSONObject();
      doNotMatch.put("defaultMessage", this.messageSource.getMessage("password.noMatch", null, locale));
      errors.put(doNotMatch);
    }

    response.put("errors", errors);
    response.put("accept", "0");
    if (errors.length() != 0) {
      return response;
    }
    response.put("accept", "1");
    this.passwordTokenRepository.delete(passwordToken);
    User user = this.getUserById(userID);
    user.setSalt(BCrypt.gensalt(12));
    user.setPassword(BCrypt.hashpw(newPassword.getNewPassword1(), user.getSalt()));
    this.saveUser(user);
    return response;
  }

  public Boolean tokenInvalid(String userID, String token) {
    PasswordToken passwordToken = this.passwordTokenRepository.findByUserID(userID);
    if (passwordToken == null)
      return true;
    if (!passwordToken.getToken().equals(token))
      return true;
    return passwordToken.getExpirationTime().getTime() < System.currentTimeMillis();
  }

  public UserDTO getUserDTO(User user) {
    UserDTO convertedUser = new UserDTO(user);
    convertedUser.fixCreatedBy(
        this.checkUserById(convertedUser.getCreatedBy()) ? this.getUserById(convertedUser.getCreatedBy()) : null);
    convertedUser.fixUpdatedBy(
        this.checkUserById(convertedUser.getUpdatedBy()) ? this.getUserById(convertedUser.getUpdatedBy()) : null);
    return convertedUser;
  }

  public Page<Map<String, String>> getPage(Pageable pageable, Authentication auth, String teamName, String filterField,
      String filterText) {
    if (filterText.equals("")) {
      filterText = ".";
    }
    Page<User> userList;
    if (!teamName.equals("")) {
      userList = this.repository.findByTeam(teamName, filterField, filterText, pageable);
    } else {
      userList = this.repository.findByEmailNot(auth.getName(), filterField, filterText, pageable);
    }

    Page<Map<String, String>> userTablelist = userList.map(new Function<User, Map<String, String>>() {
      @Override
      public Map<String, String> apply(User user) {
        Map<String, String> transferred = new HashMap<String, String>();
        transferred.put("id", user.getId());
        transferred.put("role", user.getRole().equals("ROLE_ADMIN") ? "Administrator" : "User");
        transferred.put("firstName", user.getFirstName());
        transferred.put("lastName", user.getLastName());
        transferred.put("emailAddress", user.getEmailAddress());
        String teams = "";
        for (String teamName : user.getTeams()) {
          teams += teamName + "; ";
        }
        transferred.put("teams", teams);
        return transferred;
      }
    });
    return userTablelist;
  }

}
