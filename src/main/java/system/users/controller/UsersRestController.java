package system.users.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import system.exceptions.ForbiddenException;
import system.exceptions.ResourceNotFoundException;
import system.users.model.PasswordChange;
import system.users.model.UserDTO;
import system.users.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping(produces = {"*/*;charset=UTF-8"})
public class UsersRestController {

  @Autowired
  UserService userService;

  @Autowired
  private MessageSource messageSource;

  private static final Logger logger = LoggerFactory.getLogger(UsersRestController.class);

  @RequestMapping("/user")
  public Principal user(Principal principal) {
    return principal;
  }

  @RequestMapping("/userDetails")
  public UserDTO userDetail(Principal principal) {
    return userService.getUserDTO(userService.getUserByEmailAddress(principal.getName()));
  }

  @RequestMapping("/users/{id}")
  public UserDTO userById(Authentication auth, @PathVariable String id) {
    if (!userService.getUserByEmailAddress(auth.getName()).getRole().equals("ROLE_ADMIN")) {
      throw new ForbiddenException();
    }
    if (!userService.checkUserById(id)) {
      throw new ResourceNotFoundException();
    }
    return userService.getUserDTO(userService.getUserById(id));
  }

  @RequestMapping(value = "/users", method = RequestMethod.GET)
  public @ResponseBody Page<Map<String, String>> usersList(
      @PageableDefault(page = 0, size = Integer.MAX_VALUE) Pageable pageable, @RequestParam("team") String teamName,
      @RequestParam("filterField") String filterField, @RequestParam("filterValue") String filterValue,
      Authentication auth) {
    if (!userService.getUserByEmailAddress(auth.getName()).getRole().equals("ROLE_ADMIN")) {
      throw new ForbiddenException();
    }
    return userService.getPage(pageable, auth, teamName, filterField, filterValue);
  }

  @RequestMapping(value = "/users", method = RequestMethod.POST)
  public ResponseEntity<String> addUser(@RequestBody @Valid UserDTO newUserDTO, Authentication auth, Locale locale) {
    if (!userService.getUserByEmailAddress(auth.getName()).getRole().equals("ROLE_ADMIN")) {
      throw new ForbiddenException();
    }
    JSONObject response = new JSONObject();
    JSONArray errors = new JSONArray();
    if (!userService.uniqueEmail(newUserDTO.getEmailAddress())) {
      JSONObject error = new JSONObject();
      error.put("defaultMessage", this.messageSource.getMessage("email.registered", null, locale));
      errors.put(error);
    }
    response.put("errors", errors);
    if (errors.length() != 0) {
      return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
    }
    userService.addUser(newUserDTO, auth);
    return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
  }

  @RequestMapping(value = "/users/{id}", method = RequestMethod.PATCH) //, produces = "application/json; charset=utf-8")
  public ResponseEntity<String> updateUser(@PathVariable String id, @RequestBody @Valid UserDTO edited,
      BindingResult bindingResult, Authentication auth, Locale locale) {
    if (!userService.getUserByEmailAddress(auth.getName()).getRole().equals("ROLE_ADMIN")) {
      throw new ForbiddenException();
    }
    if (!userService.checkUserById(id)) {
      throw new ResourceNotFoundException();
    }
    edited.setId(id);
    JSONObject response = userService.validate(edited, userService.getUserById(id).getEmailAddress(), bindingResult,
        locale);
    if (response.get("accept").toString().equals("0")) {
      return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
    }
    userService.updateUser(edited, userService.getUserById(id), auth);
    return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
  }

  @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
  public Boolean deleteUserSure(@PathVariable String id, Authentication auth) {
    if (!userService.getUserByEmailAddress(auth.getName()).getRole().equals("ROLE_ADMIN")) {
      throw new ForbiddenException();
    }
    if (!userService.checkUserById(id)) {
      throw new ResourceNotFoundException();
    }
    userService.deleteUserById(id);
    return true;
  }

  @RequestMapping(value = "/users/password", method = RequestMethod.PUT)
  public ResponseEntity<String> passwordChange(Authentication auth, @RequestBody @Valid PasswordChange change,
      Locale locale) {
    JSONObject response = userService.changePassword(auth.getName(), change, locale);
    if (response.get("accept").equals("0")) {
      return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
  }

  @RequestMapping(value = "/users/resetPassword", method = RequestMethod.GET)
  public Boolean resetPassword(HttpServletRequest request, @RequestParam("id") String id,
      @RequestParam("token") String token) {
    if (userService.tokenInvalid(id, token)) {
      throw new ResourceNotFoundException();
    }
    return true;
  }

  @RequestMapping(value = "/users/resetPassword", method = RequestMethod.POST)
  public Boolean resetPassword(HttpServletRequest request, @RequestBody String email) {
    if (userService.getUserByEmailAddress(email) == null) {
      throw new ResourceNotFoundException();
    }

    userService.resetPassword(email, true);
    return true;
  }

  @RequestMapping(value = "/users/resetPassword", method = RequestMethod.PUT)
  public ResponseEntity<String> resetPasswordNew(@RequestParam("id") String userID, @RequestParam("token") String token,
      @RequestBody @Valid PasswordChange newPassword, Locale locale) {
    JSONObject response = userService.userResetPassword(userID, token, newPassword, locale);
    if (response.get("accept").equals("0")) {
      return new ResponseEntity<String>(response.toString(), HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
  }

}
