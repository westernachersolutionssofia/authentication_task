package system.users.model;

import javax.validation.constraints.NotEmpty;

/**
 * Created by ivan on 7/22/16.
 */
public class PasswordChange {

    @NotEmpty(message = "{field.empty}")
    private String oldPassword, newPassword1, newPassword2;

    public PasswordChange(String oldPassword, String newPassword1, String newPassword2) {
        this.oldPassword = oldPassword;
        this.newPassword1 = newPassword1;
        this.newPassword2 = newPassword2;
    }

    PasswordChange() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword1() {
        return newPassword1;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setNewPassword1(String newPassword1) {
        this.newPassword1 = newPassword1;
    }

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }
}
