package system.users.model;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class User {
  private String firstName, lastName, emailAddress, password, role, salt;
  private Date dateOfBirth;
  private List<String> teams;

  @Id
  private String id;

  private String createdBy, updatedBy;
  private Date beganWorkingOn, lastRetrieved;
  private String fullName, position;

  public User() {
    this.firstName = "-";
    this.lastName = "-";
    this.teams = new ArrayList<String>();
    this.lastRetrieved = Calendar.getInstance().getTime();
    this.beganWorkingOn = lastRetrieved;
    this.createdBy = "-1";
    this.updatedBy = "-1";
    this.fullName = "";
    this.position = "";
  }

  public User(User user) {
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.emailAddress = user.getEmailAddress();
    this.role = user.getRole();

    this.teams = user.getTeams();
    this.beganWorkingOn = user.getBeganWorkingOn();
    this.createdBy = user.getCreatedBy();
    this.updatedBy = user.getUpdatedBy();
    this.fullName = user.getFullName();
    this.position = user.getPosition();
  }

  public String getSalt() {
    return salt;
  }

  public void setSalt(String salt) {
    this.salt = salt;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public List<String> getTeams() {
    return teams;
  }

  public void setTeams(List<String> teams) {
    this.teams = teams;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getBeganWorkingOn() {
    return beganWorkingOn;
  }

  public void setBeganWorkingOn(Date beganWorkingOn) {
    this.beganWorkingOn = beganWorkingOn;
  }

  public Date getLastRetrieved() {
    return lastRetrieved;
  }

  public void setLastRetrieved(Date lastRetrieved) {
    this.lastRetrieved = lastRetrieved;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public void update(UserDTO updated) {
    if (updated.getFirstName() != null) {
      this.firstName = updated.getFirstName();
    }
    if (updated.getLastName() != null) {
      this.lastName = updated.getLastName();
    }
    if (updated.getEmailAddress() != null) {
      this.emailAddress = updated.getEmailAddress();
    }
    if (updated.getRole() != null) {
      this.role = updated.getRole();
    }
    if (updated.getDateOfBirth() != null) {
      this.dateOfBirth = updated.getDateOfBirth();
    }
    if (updated.getBeganWorkingOn() != null) {
      this.beganWorkingOn = updated.getBeganWorkingOn();
    }
    if (updated.getTeams() != null) {
      this.teams = updated.getTeams();
    }
    if (updated.getFullName() != null) {
      this.fullName = updated.getFullName();
    }
    if (updated.getPosition() != null) {
      this.position = updated.getPosition();
    }
  }

  public void removeTeam(String teamName) {
    if (teams.contains(teamName)) {
      teams.remove(teamName);
    }
  }

  public void updateTeam(String oldName, String newName) {
    teams.set(teams.indexOf(oldName), newName);
  }

}
