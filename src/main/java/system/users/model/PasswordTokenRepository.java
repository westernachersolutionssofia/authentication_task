package system.users.model;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ivan on 12/27/16.
 */
public interface PasswordTokenRepository extends MongoRepository<PasswordToken, String> {
    PasswordToken findByUserID(String userID);
}
