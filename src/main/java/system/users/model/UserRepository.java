package system.users.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserRepository extends MongoRepository<User, String> {

  User findByEmailAddressIgnoreCase(String emailAddress);

  Long countByEmailAddressIgnoreCase(String emailAddress);

  @Query(value = "{ $and: [{'teams': { $in: [?0] }}, {?1: {$regex: ?2, $options: 'i'}}]}")
  Page<User> findByTeam(String teamName, String field, String text, Pageable pageable);

  @Query(value = "{ $and: [{?1 : {$regex: ?2 , $options: 'i' }}, {'emailAddress' : { $not : {$eq: ?0 }}}]}")
  Page<User> findByEmailNot(String emailAddress, String field, String text, Pageable pageable);
}
