package system.users.model;

import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.UUID;

/**
 * Created by ivan on 12/27/16.
 */
public class PasswordToken {

    @Id
    private String id;

    private String userID;
    private String token;
    private Date expirationTime;

    PasswordToken (){

    }

    public PasswordToken(String userID, Integer durationInHours) {
        this.userID = userID;
        this.token = UUID.randomUUID().toString();
        this.expirationTime = new Date(System.currentTimeMillis() + durationInHours * 60 * 60 * 1000L);
    }

    public String getToken() {
        return token;
    }

    public String getUserID() {
        return userID;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }
}
