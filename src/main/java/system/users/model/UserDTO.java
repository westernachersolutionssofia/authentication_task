package system.users.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

public class UserDTO {

  @Size(min = 2, message = "{field.minlen2}")
  @Size(max = 512, message = "{field.maxlen512}")
  private String firstName, lastName;

  @NotEmpty(message = "{field.empty}")
  @Email(message = "{email.proper}")
  @Size(max = 1024, message = "{field.maxlen1024}")
  private String emailAddress;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  @NotNull(message = "{field.empty}")
  @Past(message = "{date.past}")
  private Date dateOfBirth, beganWorkingOn;

  @NotEmpty(message = "{field.empty}")
  private String password;

  @NotNull(message = "{field.empty}")
  private String role;

  private String id;

  private List<String> teams;

  private String createdBy, updatedBy;

  @Size(min = 2, message = "{field.minlen2}")
  @Size(max = 1024, message = "{field.maxlen1024}")
  private String fullName, position;

  public UserDTO() {
  }

  public UserDTO(User user) {
    convert(user);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public Date getBeganWorkingOn() {
    return beganWorkingOn;
  }

  public void setBeganWorkingOn(Date beganWorkingOn) {
    this.beganWorkingOn = beganWorkingOn;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public List<String> getTeams() {
    return teams;
  }

  public void setTeams(List<String> teams) {
    this.teams = teams;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public User toUser() {
    User newUser = new User();
    newUser.setId(id);
    newUser.setFirstName(firstName);
    newUser.setLastName(lastName);
    newUser.setEmailAddress(emailAddress);
    newUser.setDateOfBirth(dateOfBirth);
    newUser.setBeganWorkingOn(beganWorkingOn);
    newUser.setPassword(password);
    newUser.setRole(role);
    newUser.setTeams(teams);
    newUser.setFullName(fullName);
    newUser.setPosition(position);
    return newUser;
  }

  public void convert(User user) {
    this.id = user.getId();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.emailAddress = user.getEmailAddress();
    this.dateOfBirth = user.getDateOfBirth();
    this.beganWorkingOn = user.getBeganWorkingOn();
    this.role = user.getRole();
    this.teams = user.getTeams();
    this.createdBy = user.getCreatedBy();
    this.updatedBy = user.getUpdatedBy();
    this.fullName = user.getFullName();
    this.position = user.getPosition();
  }

  public void fixCreatedBy(User user) {
    if (user == null) {
      this.createdBy = "-";
    } else {
      this.createdBy = user.getFirstName() + ' ' + user.getLastName();
    }
  }

  public void fixUpdatedBy(User user) {
    if (user == null) {
      this.updatedBy = "-";
    } else {
      this.updatedBy = user.getFirstName() + ' ' + user.getLastName();
    }
  }
}
