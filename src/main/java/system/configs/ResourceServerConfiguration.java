package system.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import system.authentication.MyLogoutSuccessHandler;

/**
 * Created by ivan on 7/21/16.
 */
@Configuration
@EnableResourceServer
@Order(2)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

  private static final Logger logger = LoggerFactory.getLogger(ResourceServerConfiguration.class);

  @Bean
  public MyLogoutSuccessHandler myLogoutSuccessHandler() {
    return new MyLogoutSuccessHandler();
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {

    http.cors().and().csrf().disable();
    http.authorizeRequests().antMatchers("/login", "/logout").permitAll();
    http.logout().logoutSuccessHandler(myLogoutSuccessHandler());
  }

}
