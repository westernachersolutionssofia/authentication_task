# Task Overview

Your team is given the task to build a web application that has an authentication and authorization module.

After some time the sub-team responsible for the module comes to you stating they are ready. In order to help you understand their implementation they provide you also with a small POC application, that consist of:

*   login mechanism
*   logout mechanism
*   every user can change their password
*   every user can see a list of the users in the application
*   only admin users can edit users from the list
*   only admin users can delete users from the list 

# Architecture & System Requirements
The implementation of the application uses the Spring Boot framework, REST for communication and a MongoDB database for storage.

The system requirements for the application are the following:

*   Java Development Kit (JDK) at least version 8.0
*   Gradle build tool
*   MongoDB installed and running

# Your Goal
You have to be able to read and understand the implementation, spot the technical and functional depths in the whole solution (we would check on what do you think a wholistic solution of this task is) and compile a report with suggestions what can be optimized and how.

It would be great to also explain the approach you would be taking and what your communication with your teammates would be.

Note: We don't need static code analysis, we need you to provide constructive feedback on what can be approved and how 